import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import scala.math.pow
import scala.math.sqrt

object KMeans {
  type Point = (Double,Double)

  var centroids: Array[Point] = Array[Point]()

  def distance(p: Point, c: Point): Double= {
    val dist = sqrt ( pow((p._1 - c._1), 2) + pow((p._2 - c._2), 2) )
    dist
  }

  def average(pointsArray: Array[Point]): Point= {
    var newCentroid = (0.0,0.0)
    for( i <- 0 to pointsArray.length -1 ) {
      newCentroid = ( (pointsArray(i)._1+newCentroid._1) , (pointsArray(i)._2+newCentroid._2))
    }
    newCentroid = (newCentroid._1 / pointsArray.length, newCentroid._2 / pointsArray.length)
    newCentroid
  }

  def main(args: Array[ String ]) {
    val conf = new SparkConf().setAppName("KmeansJoin");
    val sc = new SparkContext(conf);

    val points = sc.textFile(args(0)).map( line => { val s = line.split(",")
                                                  new Point(s(0).toDouble, s(1).toDouble)
                                                           } ).collect

    /* read initial centroids from centroids.txt */
    val centroidFile =sc.textFile(args(1))
    var array = centroidFile.map( line => { val p = line.split(",")
      new Point(p(0).toDouble, p(1).toDouble)
    } ).collect
    centroids = array;

    for ( i <- 1 to 5 ) {
      val cs = sc.broadcast(centroids)
      val closest = points.map { p => (cs.value.minBy(distance(p, _)), p)
      }
      val closestRDD = sc.parallelize(closest)
      centroids = closestRDD.groupByKey().map {
        /* ... calculate a new centroid ... */
        case (c, pointsInACluster) =>
          val newCentroid = average(pointsInACluster.toArray)
          newCentroid
      }.collect()
      }
      centroids.foreach(println)
  }
}
